# tic-tac-toe game

Multiplayer, uses web sockets.

<img src="print-screen.png" alt="screenshot">

screen shot showing game played on web and cli

## Requirement

`node --version v9.8.0`

## Build

`npm i`

`npm run build-client`

## Play

`npm run play` is required

open http://localhost:8080 to play web version

`npm run cli` to play on the cli, *use another command prompt from the first one*.

## Code

```
turn game engine exports 2 functions
createGameLobby and processInput. createGameLobby returns a lobby object that stores all game information, who is currently playing, is the game finished, how many players etc.


But it does not know about tic tac toe rules. Instead it takes as parameter 3 game specific functions. processInput and getGameResults and restartGame. everytime you call  processInput(gameLobby, input), it does basic validation then passes the meaningfull input down to the tic tac toe processInput




gameLobby.gameData = gameLobby.processInput(gameLobby.gameData, input.playerNumber, input.gameAction);


the turnGameEngine expects that restartGame returns a fresh game and getGameResults returns something like

{
    finished: false,
    winner: undefined,
    tied: undefined,
}


if processInput throws then the game state does not change and the error is stored inside gameLobby.error and send back to the client who tried to cheat.


The server creates a new gameLobby everytime 2 player connect, but the clients only create 1 gameLobby. the same code is used from turn game engine and tic tac toe on both clients and the server.


Whenever a clients plays it first sends the action to the server and only plays out the action locally with processInput(gameLobby, data); when the server responds with {action: executedVerifiedTurn, ...} additionaly it uses renderDep.renderGame(renderGame) on the client , which on the web version updates the DOM and on the cli uses console log.


I am not sure if it makes more sense for you now, but essentially I tried to maximize code reuse and for that I used to depend on abstractions which in a way makes it harder to read. I also put focus on server authority so that even if clients try to cheat by crafting a message it cannot because it is verified server side first.

```
