/*
server side, entry file
*/
import {createGameLobby, processInput} from "./turn-game-engine/turn-game-engine.mjs";
import {createNewGrid, ticTacToeProcessInput,
     playersAmount, getResults} from "./tic-tac-toe-game/tic-tac-toe-game.mjs";
import {start as startServer, handlers} from "./server/server.mjs";
import {start as startTcpServer, handlers as tcpHandlers} from "./server/tcp.mjs";


let waitingPlayer = undefined;
const players = {};

const sendRestart = function (playerPair, startingPlayer) {
    playerPair.forEach(function (player) {
        player.channel.send({
            action: `restart`,
            data: {
                playerNumber: player.playerNumber,
                startingPlayer
            }
        });
    });
};

const sendQueued = function (player) {
    player.channel.send({
        message: `Queued on the match making system, waiting ...`
    });
};

const randomPlayerStart = function () {
    return Math.floor(Math.random() * playersAmount);
};

handlers.onPlayerConnected = function (channel) {
    console.log(`handle connect from player ${channel.id}`);
    const newPlayer = {
        id: channel.id,
        channel,
        wins: 0, // todo
        playerNumber: undefined,
        gameLobby: undefined
    };
    players[newPlayer.id] = newPlayer;
    if (waitingPlayer === undefined) {
        // queued on the matchmaking
        waitingPlayer = newPlayer;
        sendQueued(newPlayer);
        return;
    }
    // else start a new game
    const opponent = waitingPlayer;
    waitingPlayer = undefined;

    const playerPair = [opponent, newPlayer];
    const gameLobby = createGameLobby({
        playersAmount,
        players: playerPair,

        processInput: ticTacToeProcessInput,
        getGameResults: getResults,
        restartGame: createNewGrid,
        startingPlayer: randomPlayerStart()
    });

    opponent.gameLobby = gameLobby;
    newPlayer.gameLobby = gameLobby;

    opponent.playerNumber = 0;
    newPlayer.playerNumber = 1;
    sendRestart(playerPair, gameLobby.currentlyPlaying);

};

handlers.onPlayerDisconnected = function (id) {
    console.log(`handle disconnect from player ${id}`);
    const leavingPlayer = players[id];
    /*
    scenario 1 the player is on the waiting sit
    */
    if (waitingPlayer === leavingPlayer) {
        waitingPlayer = undefined;
        delete players[id];
        return;
    }
    /*
    scenario 2 the player is in game with someone
    */
    const gameLobby = leavingPlayer.gameLobby;
    gameLobby.players.forEach(function (player) {
        player.gameLobby = undefined;
        if (player !== leavingPlayer) {
            /*
             the player who is left alone is now waiting
            */
            waitingPlayer = player;
            sendQueued(player);
        }
    });
    gameLobby.players = undefined;

    /*
    scenario 3 the player is being added while being removed is impossible if a single instance of this file is launched
    */
};

handlers.onPlayerSendMessage = function (id, message) {
    const from = players[id];
    const gameLobby = from.gameLobby;
    if (!gameLobby) {
        from.channel.send({
            message: `Wait for a match to start playing`
        });
        return;
    }
    if (gameLobby.results.finished) {
        // restart
        gameLobby.resetLobby({
            startingPlayer: randomPlayerStart()
        });
        sendRestart(gameLobby.players, gameLobby.currentlyPlaying);
        return;
    }
    message.playerNumber = from.playerNumber;
    processInput(gameLobby, message);
    if (gameLobby.error) {
        from.channel.send({message: gameLobby.error});
        return;
    }
    // else
    gameLobby.players.forEach(function (player) {
        player.channel.send({
            action: `executedVerifiedTurn`,
            data: message
        });
    });

};


tcpHandlers.onPlayerConnected = handlers.onPlayerConnected;
tcpHandlers.onPlayerDisconnected = handlers.onPlayerDisconnected;
tcpHandlers.onPlayerSendMessage = handlers.onPlayerSendMessage;

/*
Start :)
*/
startServer();
startTcpServer();
