export { handleStatic };

import fs from "fs";

const staticsPath = `./webclient`;

const staticResponses = {
    [`/`]: {
        [`file`]: `${staticsPath}/html/index.html`,
        [`Content-Type`]: `text/html`
    },
    [`/game.css`]: {
        [`file`]: `${staticsPath}/css/game.css`,
        [`Content-Type`]: `text/css`
    },
    [`/webclient.js`]: {
        [`file`]: `${staticsPath}/js/built/webclient.iife.js`,
        [`Content-Type`]: `text/javascript`
    }
};

const handleStatic = (request, response) => {
    if (staticResponses.hasOwnProperty(request.url)) {
        response.writeHead(200, { [`Content-Type`]: staticResponses[request.url][`Content-Type`] });
        fs.createReadStream(staticResponses[request.url][`file`]).pipe(response);
    } else {
        response.writeHead(404, { [`Content-Type`]: `text/plain` });
        response.end(`404 Wrong url - not found`);
    }
    return;
};
