export { start, handlers };

import http from "http";
import fs from "fs";
import WebSocket from "ws";

import { handleStatic } from "./static-handler.mjs";


const PORT = 8080;
const WEBSOCKET_PORT = 8081;

const handlers = {
    onPlayerConnected: undefined,
    onPlayerDisconnected: undefined,
    onPlayerSendMessage: undefined,
};


const server = http.createServer((request, response) => {
    // console.log(request.url);
    if (request.method === `GET`) {
        handleStatic(request, response);
        return;
    }
    response.setHeader(`Content-Type`, `text/plain`);
    response.writeHead(405);
    response.end(`Only use GET please`);
});



const start = function () {
    const wss = new WebSocket.Server({ port: WEBSOCKET_PORT });
    let nextId = 0;
    wss.on(`connection`, function (ws) {
        const id = nextId;
        nextId += 1;

        ws.on(`message`, function (message) {
            handlers.onPlayerSendMessage(id, JSON.parse(message));
        });

        ws.on(`close`, function (event) {
            handlers.onPlayerDisconnected(id, event.code);
        });

        handlers.onPlayerConnected({
            // only expose ws.send and id nothing else
            id,
            send: function (message) {
                ws.send(JSON.stringify(message));
            }
        });
    });
    server.listen(PORT);
    console.log(`Listening on ${PORT}`);
};
