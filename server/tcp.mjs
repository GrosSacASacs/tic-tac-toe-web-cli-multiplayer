export {start, handlers};

import net from "net";

const separator = `\0`;
const TCP_PORT = 8082;
const handlers = {
    onPlayerConnected: undefined,
    onPlayerDisconnected: undefined,
    onPlayerSendMessage: undefined,
};

const tcpConnections = {};

const tcpServer = net.createServer(function (socket) {
    const id = `${socket.remotePort}${socket.remoteAddress}${Date.now()}`;
    socket.id = id
    tcpConnections[id] = socket;

    socket.on(`data`, function (data) {
        const rawString = String(data);
        const split = rawString.split(separator);
        split.filter(Boolean).forEach(oneMessage => {
            handlers.onPlayerSendMessage(id, JSON.parse(String(oneMessage)));
        })
    });

    socket.on(`end`, function () {
        handlers.onPlayerDisconnected(id);
    });

    socket.on(`error`, function () {
        handlers.onPlayerDisconnected(id);
    });

    handlers.onPlayerConnected({
        // only expose ws.send and id nothing else
        id,
        send: function (message) {
            socket.write(`${JSON.stringify(message)}${separator}`);
        }
    });

});

const start = function () {
    tcpServer.listen(TCP_PORT);
    console.log(`TCP CLI at port ${TCP_PORT}`);
};
