export {actions, renderDep, gameLobby, myself};

import {createNewGrid, ticTacToeProcessInput, playersAmount, getResults} from "../tic-tac-toe-game/tic-tac-toe-game.mjs";
import {createGameLobby, processInput} from "../turn-game-engine/turn-game-engine.mjs";


const renderDep = {
    // dependency that needs to be injected
    renderGame: undefined
};

// represents the person playing on this computer
const myself = {
    playerNumber: undefined
};

const opponent = {
    playerNumber: `not necessery now`
};

// create a single lobby here
// gameLobby.resetLobby must be called to have synced startingPlayer
const gameLobby = createGameLobby({
    playersAmount,
    players: [myself, opponent],

    processInput: ticTacToeProcessInput,
    getGameResults: getResults,
    restartGame: createNewGrid,
    startingPlayer: 0
});


const actions = {
    restart: function (data) {
        gameLobby.resetLobby({
            startingPlayer: data.startingPlayer
        });
        myself.playerNumber = data.playerNumber;
        renderDep.renderGame(gameLobby, myself);
    },
    executedVerifiedTurn: function (data) {
        processInput(gameLobby, data);
        renderDep.renderGame(gameLobby, myself);
    }
};
