import d from "../../node_modules/dom99/built/dom99Module.js";

export {renderGame};


const renderGame = function (gameLobby, myself) {
    gameLobby.gameData.forEach(function (value, position) {
        let symbol;
        if (value === 0) {
            symbol = `O`;
        } else if (value === 1) {
            symbol = `X`;
        } else {
            symbol = ``;
        }
        d.feed(position, symbol);
    });
    if (gameLobby.results.finished) {
        if (gameLobby.results.tied) {
            d.feed(`gametext`, `Draw !`);
        } else {
            if (gameLobby.results.winner === myself.playerNumber) {
                d.feed(`gametext`, `You win !`);
            } else {
                d.feed(`gametext`, `You loose !`);
            }
        }
    } else {
        if (gameLobby.currentlyPlaying === myself.playerNumber) {
            d.feed(`gametext`, `You have to play !`);
        } else {
            d.feed(`gametext`, `Wait for your opponent...`);
        }
    }
};
