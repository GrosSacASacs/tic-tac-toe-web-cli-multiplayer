/*
bridge between client-server
possible improvement: check if a move is valid client side
and immediate visual feedback
*/

import d from "../../node_modules/dom99/built/dom99Module.js";
import { createIntelligence, learn, decide } from "../../node_modules/qlearn/source/qlearn.js";
import { actions, renderDep, gameLobby } from "../../common/play-tic-tac-toe-client.mjs";
import { processInput } from "../../turn-game-engine/turn-game-engine.mjs";
import { renderGame } from "./rendering.js";
import { deepEqual } from "../../node_modules/utilsac/deep.js";
renderDep.renderGame = renderGame;

const webSocketLocation = `ws://${location.hostname}:8081/`;
let open = false;
let socket;

let clickHander = function () {
    d.feed(`gametext`, `select a playmode`);

};
d.start({
    clickGrid: function (event) {
        clickHander(event)
    },
    startPvP: function () {
        if (open) {
            return;
        }
        socket = new WebSocket(webSocketLocation);

        // Connection opened
        socket.addEventListener(`open`, function (event) {
            open = true;
        });

        // Listen for messages
        socket.addEventListener(`message`, function (event) {

            const object = JSON.parse(event.data);
            if (object.message) {
                d.feed(`gametext`, object.message);
                console.log(object.message);
            }
            if (object.action) {
                console.log(object.action);
                console.log(object.data);
                actions[object.action](object.data);
            }
        });

        socket.addEventListener(`error`, function (error) {
            console.log(`error`, error);
        });

        const socketSend = function (message) {
            socket.send(JSON.stringify(message));
        };

        clickHander = function (event) {
            if (!open) {
                d.feed(`gametext`, `Connection not open`);
            }
            const position = Number(event.target.getAttribute(`data-id`));
            d.feed(`gametext`, `Sending ${position}`);
            socketSend({
                gameAction: {
                    position: position
                }
            });
        };
    },
    startPvAI: function () {
        d.feed(`gametext`, `You start`);
        const myself = {
            playerNumber: 0
        };
        actions.restart({
            playerNumber: 0,
            startingPlayer: 0
        });
        const intelligence = createIntelligence();
        let previousStateActions = "not started";
        let previousAction = "not started";
        let previousActions;
        clickHander = function (event) {
            const position = Number(event.target.getAttribute(`data-id`));

            let reward = 0;
            if (gameLobby.results.finished) {
                // restart
                if (gameLobby.results.winner === 0) {
                    reward = -1;
                } else if (gameLobby.results.winner === 1) {
                    reward = 1
                } else {
                    reward = 0.1; // if both players are good this is positive
                }

                learn({
                    intelligence,
                    previousStateActions,
                    stateActions: gameLobby.results.winner,
                    previousAction,
                    previousActions,
                    reward,
                });
                gameLobby.resetLobby({
                    startingPlayer: 0
                });

                renderGame(gameLobby, myself);
                previousStateActions = "not started";
                previousAction =  "not started";
                return;
            }
            processInput(gameLobby, {
                playerNumber: 0,
                gameAction: {
                    position: position
                }
            });
            if (gameLobby.error) {
                d.feed(`gametext`, gameLobby.error);
                return;
            }
            if (gameLobby.results.finished) {
                // restart
                renderGame(gameLobby, myself)
                return;
            }
            const actionNames = Object.entries(gameLobby.gameData).filter(([position, player]) => {
                return player === -1;
            }).map(([position, player]) => {
                return position;
            }).map(Number)
            const stateActions = String(gameLobby.gameData);
            if (previousStateActions !== "not started") {
                learn({
                    intelligence,
                    previousStateActions,
                    stateActions,
                    previousAction,
                    previousActions,
                    reward,
                });
            }

            previousStateActions = stateActions;
            const actionName = decide({ intelligence, stateActions, actionNames });
            previousActions = actionNames;
            console.log(intelligence);
            console.log(actionName);
            previousAction = actionName;
            processInput(gameLobby, {
                playerNumber: 1,
                gameAction: {
                    position: actionName
                }
            });
            if (gameLobby.error) {
                d.feed(`gametext`, `invalid move from AI ${gameLobby.error}`);
                renderGame(gameLobby, 0)
                return;
            }
            renderGame(gameLobby, myself)
        };

    },
    startAIvAI: function () {
        d.feed(`gametext`, `Click to start`);
        let paused = true;

        let previousStateActions;
        let previousAction;
        let previousStateActions2;
        let previousAction2;
        const intelligence = createIntelligence();
        const intelligence2 = createIntelligence();
        const agents = [{
            intelligence: intelligence,
            previousStateActions: previousStateActions,
            previousAction: previousAction,

        }, {
            intelligence: intelligence2,
            previousStateActions: previousStateActions2,
            previousAction: previousAction2,
        }];
        let requestAnimationFrameId;
        clickHander = function (event) {
            paused = !paused;
            if (!paused) {
                requestAnimationFrameId = requestAnimationFrame(playTurn);
            } else {
                cancelAnimationFrame(requestAnimationFrameId);
            }
        };

        actions.restart({
            playerNumber: 0,
            startingPlayer: 0
        });
        const playTurn = function () {
            requestAnimationFrameId = requestAnimationFrame(playTurn);

            let reward = 0;
            let reward2 = 0;
            if (gameLobby.results.finished) {
                console.log(gameLobby.results.winner)
                // restart
                if (gameLobby.results.winner === 0) {
                    reward = -1;
                } else if (gameLobby.results.winner === 1) {
                    reward = 1
                }
                reward2 = -reward;
                learn({
                    intelligence,
                    previousStateActions,
                    stateActions: ``,
                    previousAction,
                    actionNames: [],
                    reward,
                });
                learn({
                    intelligence: intelligence2,
                    previousStateActions: previousStateActions2,
                    stateActions: ``,
                    previousAction: previousAction2,
                    actionNames: [],
                    reward: reward2,
                });
                gameLobby.resetLobby({
                    startingPlayer: 0
                });

                renderGame(gameLobby, 0)
                return;
            }
            const { currentlyPlaying } = gameLobby;
            console.log(currentlyPlaying)
            const actionNames = Object.entries(gameLobby.gameData).filter(([position, player]) => {
                return player === -1;
            }).map(([position, player]) => {
                return position;
            }).map(Number);
            const stateActions = String(gameLobby.gameData);

            console.log(actionNames);
            console.log(stateActions);


            learn({
                intelligence: agents[currentlyPlaying].intelligence,
                previousStateActions: agents[currentlyPlaying].previousStateActions,
                stateActions,
                previousAction: agents[currentlyPlaying].previousAction,
                actionNames,
                reward,
            });

            agents[currentlyPlaying].previousStateActions = stateActions;
            const actionName = decide({ intelligence: agents[currentlyPlaying].intelligence, stateActions, actionNames });
            agents[currentlyPlaying].previousAction = actionName;
            processInput(gameLobby, {
                playerNumber: currentlyPlaying,
                gameAction: {
                    position: actionName,
                },
            });
            if (gameLobby.error) {
                renderGame(gameLobby, 0);
                d.feed(`gametext`, `invalid move from AI ${gameLobby.error}`);
                console.log(gameLobby.gameData)
                console.log(actionNames)
                console.log(actionName)
                console.log()
                console.log(Object.entries(gameLobby.gameData).filter(([position, player]) => {
                    return player === -1;
                }).map(([position, player]) => {
                    return position;
                }).map(Number))
                console.log()
                console.log(agents[currentlyPlaying].intelligence.qualityMap)
                cancelAnimationFrame(requestAnimationFrameId);
                paused = true
                return;
            }
            renderGame(gameLobby, 0);
        };
    },
});
