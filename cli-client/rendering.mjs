export {renderGame};


const renderGame = function (gameLobby, myself) {


    console.log(`\n -------- `);
    let line = `| `;
    gameLobby.gameData.forEach(function (value, position) {
        let symbol;
        if (value === 0) {
            symbol = `O`;
        } else if (value === 1) {
            symbol = `X`;
        } else {
            symbol = ` `;
        }
        line = `${line} ${symbol}`
        if ((position + 1) % 3 === 0) {
            console.log(`${line} |`);
            line = `| `;
        }
    });
    console.log(` -------- \n`);
    if (gameLobby.results.finished) {
        if (gameLobby.results.tied) {
            console.log(`Draw ! Press 0 to restart`);
        } else {
            if (gameLobby.results.winner === myself.playerNumber) {
                console.log(`You win ! Press 0 to restart`);
            } else {
                console.log(`You loose ! Press 0 to restart`);
            }
        }
    } else {
        if (gameLobby.currentlyPlaying === myself.playerNumber) {
            console.log(`You have to play !`);
        } else {
            console.log(`Waiting for the other player ... `);
        }
    }
};
