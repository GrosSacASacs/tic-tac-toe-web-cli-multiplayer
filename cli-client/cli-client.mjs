import net from "net";
import inquirer from "inquirer";

import {renderGame} from "./rendering.mjs";
import {actions, renderDep, gameLobby, myself} from "../common/play-tic-tac-toe-client.mjs";

renderDep.renderGame = renderGame;

const TARGET_HOST = `127.0.0.1`;
const TARGET_TCP_PORT = 8082;

const thisConnection = new net.Socket();
const separator = `\0`;

thisConnection.on(`data`, function (data) {
    
    const rawString = String(data);
    const split = rawString.split(separator);
    split.filter(Boolean).forEach(oneMessage => {
        const object = JSON.parse(String(oneMessage));
        if (object.message) {
            console.log(object.message);
            promptLoop();
        }
        if (object.action) {
            actions[object.action](object.data);
            promptLoop();
        }
    });
});

thisConnection.on(`error`, function (error) {
    console.log(`exiting, error`, error);
    thisConnection.destroy();
    process.exit();
});

thisConnection.connect(TARGET_TCP_PORT, TARGET_HOST);


const promptLoop = function () {
    if (gameLobby.results.finished || gameLobby.currentlyPlaying === myself.playerNumber) {
        return inquirer.prompt([{
            message: `Choose a position (0-8)`,
            type: `input`,
            name: `input`
        }]).then(function ({input}) {
        	const position = Math.floor(Math.abs(Number(input)));
            if (position > 8) {
                console.log(`bad input`);
                promptLoop();
                return;
            }
            thisConnection.write(`${JSON.stringify({
                gameAction: {
                    position
                }
            })}${separator}`);
        });
    }

};
