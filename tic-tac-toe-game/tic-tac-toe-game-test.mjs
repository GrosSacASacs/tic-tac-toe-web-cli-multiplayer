import {isGameWon, isGameTied} from "./tic-tac-toe-game.mjs";

const gridPlayer2Win = [
    -1, -1, -1,
    1, 1, 1,
    -1, -1, -1
];
const gridPlayer2NotWin = [
    -1, -1, 1,
    1, 1, -1,
    -1, 1, 1
];

console.log(`player 2 should win (true)`);
console.log(gridPlayer2Win);
console.log(isGameWon(gridPlayer2Win, 1));

console.log(`player 1 should not win (false)`);
console.log(gridPlayer2NotWin);
console.log(isGameWon(gridPlayer2NotWin, 1));

const tie = [
    0, 0, 1,
    1, 1, 0,
    0, 1, 1
];

console.log(`game should be tied (true)`);
console.log(isGameTied(tie));

const unfinished = [
    0, 0, -1,
    1, 1, -1,
    0, 1, 1
];

console.log(`game should not be tied (false)`);
console.log(isGameTied(unfinished));
