export {isGameWon, isGameTied, createNewGrid, ticTacToeProcessInput, refreshGrid, playersAmount, getResults};

/*
The 2d grid is represented with a 1d array. Each index represents one place of the grid, from left to right, top to bottom. The value represents the who has something at this position, where -1 is nobody, 0 player 1, 1 player 2
*/
const bordLength = 3;
const playersAmount = 2;
const EMPTY = -1;
const initalGrid = [
    EMPTY, EMPTY, EMPTY,
    EMPTY, EMPTY, EMPTY,
    EMPTY, EMPTY, EMPTY
];

/*
To win align 3 of the same on an axis, or diagonale
*/
const winConditions = [

];

/*
horizontal
*/
for (let i = 0; i < bordLength; i += 1) {
    const winPattern = [
        false, false, false,
        false, false, false,
        false, false, false
    ];
    for (let j = 0; j < bordLength; j += 1) {
        winPattern[(i * bordLength) + j] = true;
    }
    winConditions.push(winPattern);
}

/*
vertical
*/
for (let i = 0; i < bordLength; i += 1) {
    const winPattern = [
        false, false, false,
        false, false, false,
        false, false, false
    ];
    for (let j = 0; j < bordLength; j += 1) {
        winPattern[i + (j * bordLength)] = true;
    }
    winConditions.push(winPattern);
}

/*
diagonale
*/
winConditions.push([
        true, false, false,
        false, true, false,
        false, false, true
]);
winConditions.push([
        false, false, true,
        false, true, false,
        true, false, false
]);

const isGameWon = function (grid, player) {
    /*
    returns true if the player has won
    */
    return winConditions.some(function (winPattern) {
        return winPattern.every(function (relevant, position) {
            return !relevant || (grid[position] === player);
        });
    });
};

const isGameTied = function (grid) {
    const everyThingIsFilled = grid.every(function (value, position) {
        /* the initial grid is all empty*/
        return value !== EMPTY;
    })
    return everyThingIsFilled && !isGameWon(grid, 1) && !isGameWon(grid, 2);
};

const isGameTiedNoWin = function (grid) {
    // like the above but does not check for winner, use only if check has just been done
    const everyThingIsFilled = grid.every(function (value, position) {
        /* the initial grid is all empty*/
        return value !== EMPTY;
    })
    return everyThingIsFilled;
};

const getResults = function (grid, player, input) {
    let finished = false;
    let winner = undefined;
    let tied = undefined;
    if (isGameWon(grid, player)) {
        winner = player;
        finished = true;
        tied = false;
    } else if (isGameTiedNoWin(grid)) {
        finished = true;
        tied = true;
    }
    return {
        finished,
        winner,
        tied
    };
};

const playTurn = function (grid, player, position) {
    if (grid[position] !== EMPTY) {
        throw new Error(`Player ${player}, cannot play at position ${position}`);
    }
    grid[position] = player;
    return grid;
};

const refreshGrid = function (grid) {
    grid.forEach(function (ignored, position) {
        grid[position] = initalGrid[position];
    });
};

const createNewGrid = function () {
    const grid = []
    initalGrid.forEach(function (value) {
        grid.push(value);
    });
    return grid;
};

// some adapters for the turn-game-engine interface
const expectedKeys = [`position`];
const expectedKeysLength = expectedKeys.length;

const validateInput = function (input) {
    const inputKeys = Object.keys(input);
    if (inputKeys.length > expectedKeysLength) {
        throw `Input has unexpected keys`;
    }
    expectedKeys.forEach(function (expectedKey) {
        if (!inputKeys.includes(expectedKey)) {
            throw `Missing key ${expectedKey}`;
        }
    })
};

const ticTacToeProcessInput = function (grid, player, input) {
    validateInput(input);
    return playTurn(grid, player, input.position);
};
