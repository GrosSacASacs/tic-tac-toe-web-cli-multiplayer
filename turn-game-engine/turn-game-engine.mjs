/*
stores certain info, about who is next to play etc, uses dependency injection
*/

export { createGameLobby, processInput };

const createGameLobby = function ({
    playersAmount = 2,
    players = [],
    processInput = null,
    getGameResults = null,
    restartGame = null,
    startingPlayer = 0
} = {}) {
    /* returns a game lobby instance*/
    const gameLobby = {
        players,
        created: Date.now(),
        playersAmount,
        processInput,
        getGameResults
    };

    const resetLobby = function ({ startingPlayer = 0 } = {}) {
        gameLobby.gameData = restartGame();
        gameLobby.currentlyPlaying = startingPlayer;
        gameLobby.results = {
            finished: false,
            winner: undefined,
            tied: undefined,
            // score
        };
        gameLobby.error = undefined;
        // store all inputs to allow replays
        gameLobby.inputs = [];
    };
    resetLobby({ startingPlayer });
    gameLobby.resetLobby = resetLobby;
    return gameLobby;
};

const expectedKeys = [`playerNumber`, `gameAction`];
const expectedKeysLength = expectedKeys.length;

const validateInput = function (gameLobby, input) {
    if (input.playerNumber !== gameLobby.currentlyPlaying) {
        gameLobby.error = `Wait for ${gameLobby.currentlyPlaying} to finish first`;
    }
    const inputKeys = Object.keys(input);
    if (inputKeys.length > expectedKeysLength) {
        gameLobby.error = `Input has unexpected keys`;
        return;
    }
    expectedKeys.forEach(function (expectedKey) {
        if (!inputKeys.includes(expectedKey)) {
            gameLobby.error = `Missing key ${expectedKey}`;
        }
    });
};

const processInput = function (gameLobby, input) {
    gameLobby.error = undefined;
    validateInput(gameLobby, input);
    if (gameLobby.error) {
        return;
    }
    try {
        gameLobby.gameData = gameLobby.processInput(gameLobby.gameData, input.playerNumber, input.gameAction);
    } catch (error) {
        gameLobby.error = `Inside gameAction: ${error}`;
        return;
    }
    Object.assign(
        gameLobby.results,
        gameLobby.getGameResults(gameLobby.gameData, input.playerNumber, input.gameAction)
    );
    if (gameLobby.results.finished) {
        // console.log(`game finished`, gameLobby.results, gameLobby.gameData);
    } else {
        // rotate to next player
        gameLobby.currentlyPlaying = (gameLobby.currentlyPlaying + 1) % gameLobby.playersAmount;
    }
    gameLobby.inputs.push(input);
};
